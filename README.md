# Python Lottery Program

This Python program allows you to conduct a lottery by randomly selecting winners from a list of participants. The program prompts the user to enter the number of winners to be selected, selects the winners randomly, and displays their names with a 10-second delay between each winner.

## Usage

1. Clone the repository or download the `main.py` file.

2. Open a terminal or command prompt and navigate to the directory containing the `main.py` file.

3. Run the program by executing the following command:
`main.py`

4. Follow the prompts to enter the number of winners.

5. Winners will be randomly selected and displayed one by one with a 10-second delay between each winner.

## Requirements

- Python 3.x

## How It Works

- The program defines a list of participant names.

- The user is prompted to enter the number of winners to be selected.

- Winners are randomly selected from the list of participants using Python's `random.sample()` function.

- The program displays the names of the winners with a 10-second delay between each winner using `time.sleep()`.

- Error handling is implemented to ensure that the number of winners requested does not exceed the number of participants in the list.

## Author

- Slykereven

Feel free to modify and improve this program as needed. If you have any questions or suggestions, please feel free to contact me.