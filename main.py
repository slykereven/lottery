import random
import time

def select_winners(names, num_winners):
    if num_winners > len(names):
        raise ValueError("Number of winners cannot exceed the number of participants")
    winners = random.sample(names, num_winners)
    return winners

def main():
    names = ["Alice", "Bob", "Charlie", "David", "Eve", "Frank", "Grace", "Heather", "Irene", "Jack"]
    
    try:
        num_winners = int(input("Enter the number of winners: "))
        if num_winners <= 0:
            raise ValueError("Number of winners must be a positive integer")
    except ValueError as e:
        print("Error:", e)
        return

    print("Selecting winners...")
    try:
        winners = select_winners(names, num_winners)
    except ValueError as e:
        print("Error:", e)
        return

    print("The winners are:")
    for winner in winners:
        print(winner)
        time.sleep(10)  # Wait for 10 seconds before displaying the next winner

    print("Lottery is finished!")

if __name__ == "__main__":
    main()